﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace GameJam1 {
    public class Gun : MonoBehaviour {

        [Tooltip("Rounds per second")]
        float fireRate = 14f;
        float secondsPerRound;
        float nextTimeToFire;

        [SerializeField] int damage = 10;
        [SerializeField] Camera fpsCam;
        [SerializeField] ParticleSystem muzzleFlashParticles;
        [SerializeField] GameObject bulletImpactParticles;

        // Use this for initialization
        void Start() {
            nextTimeToFire = 0f;
        }

        // Update is called once per frame
        void Update() {
            secondsPerRound = 1f / fireRate;
            if (Time.time >= nextTimeToFire) {
                if (CrossPlatformInputManager.GetButton("Fire1")) {
                    nextTimeToFire = Time.time + secondsPerRound;
                    Shoot();
                }
            }
        }

        void Shoot() {
            muzzleFlashParticles.Play();

            RaycastHit hit;
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit)) {
                Enemy enemy = hit.transform.GetComponent<Enemy>();
                if (enemy) {
                    enemy.TakeDamage(damage);
                }

                GameObject impactGameObject = Instantiate(bulletImpactParticles, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGameObject, 0.5f);
            }
        }
    }
}
