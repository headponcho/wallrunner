﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class EnemySpawner : MonoBehaviour {

        public int enemyCount;

        [SerializeField] GameObject enemyPrefab;
        [SerializeField] Transform target;
        [SerializeField] Transform[] spawnPoints;
        [SerializeField] float spawnInterval = 5f;
        [SerializeField] int maxPerWave = 10;

        float elapsedTime = 0f;

        // Use this for initialization
        void Start() {
            enemyCount = 0;
        }

        // Update is called once per frame
        void Update() {
            elapsedTime += Time.deltaTime;

            if (elapsedTime > spawnInterval) {
                elapsedTime -= spawnInterval;
                foreach (Transform spawnPoint in spawnPoints) {
                    if (enemyCount < maxPerWave) {
                        if (Random.value < 0.5f) {
                            SpawnEnemy(spawnPoint);
                        }
                    }
                }
            }
        }

        void SpawnEnemy(Transform spawnPoint) {
            Enemy enemy = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity).GetComponent<Enemy>();
            enemy.target = target;
            enemy.enemySpawner = this;

            enemyCount++;
        }
    }
}
