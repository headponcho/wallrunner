﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class JumpPad : MonoBehaviour {

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        private void OnTriggerEnter(Collider other) {
            Rigidbody rbody = other.gameObject.GetComponent<Rigidbody>();
            if (other.gameObject.GetComponent<Player>() && rbody) {
                rbody.velocity = new Vector3(rbody.velocity.x * 10f, 100f, rbody.velocity.z * 10f);
            }
        }
    }
}
