﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class Runnable : MonoBehaviour {

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        private void OnCollisionEnter(Collision collision) {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player) {
                player.numWallsInContact++;
                player.wallContactNormal = -collision.contacts[0].normal;
                player.wallContactNormal.y = 0.0f;
            }
        }

        private void OnCollisionExit(Collision collision) {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player) {
                player.numWallsInContact--;
            }
        }
    }
}
