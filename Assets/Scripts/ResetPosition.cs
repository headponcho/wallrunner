﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class ResetPosition : MonoBehaviour {

        [SerializeField] Vector3 startPos;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            if (Input.GetKeyDown(KeyCode.R)) {
                GetComponent<Rigidbody>().position = startPos;
            }
        }
    }
}
