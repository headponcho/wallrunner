﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class Enemy : MonoBehaviour {

        [SerializeField] int health = 100;
        [SerializeField] float speed = 8f;
        [SerializeField] GameObject deathParticleSystem;

        public Transform target;
        public EnemySpawner enemySpawner;

        Rigidbody rbody;
        float timeStopped;

        private void Start() {
            rbody = GetComponent<Rigidbody>();
            timeStopped = 3f;
        }

        private void Update() {
            rbody.rotation = Quaternion.LookRotation(target.position - transform.position);

            if (Vector3.Distance(transform.position, target.position) < 10f) {
                timeStopped = 0f;
                rbody.velocity = Vector3.zero;
            } else if (timeStopped > 2f) {
                rbody.velocity = transform.forward * speed;
            }

            if (timeStopped < 2f) {
                timeStopped += Time.deltaTime;
            }
        }

        public void TakeDamage(int amount) {
            health -= amount;
            
            if (health <= 0) {
                enemySpawner.enemyCount--;
                Instantiate(deathParticleSystem, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
