﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam1 {
    public class BoostPad : MonoBehaviour {

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        private void OnTriggerEnter(Collider other) {
            Rigidbody rbody = other.gameObject.GetComponent<Rigidbody>();
            Player player = other.gameObject.GetComponent<Player>();
            if (player && rbody && player.IsTouchingWall()) {
                Vector3 boostVelocity = rbody.velocity;
                boostVelocity.x *= 2f;
                boostVelocity.z *= 2f;
                boostVelocity.y *= 5f;

                if (player.IsFalling()) {
                    boostVelocity.y = 100f;
                }

                rbody.velocity = boostVelocity;
            }
        }
    }
}
